package at.running.platform.ftp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import at.running.platform.common.model.Help;


@RunWith(SpringRunner.class)
@SpringBootTest
public class FTPControllerTests {
	
	@Autowired
	private FTPController ftpController;
	
	@Test
	public void help() {
		List<Help> helpers = ftpController.help();
		assertTrue("No Helper Defintion found", helpers.size() > 0);
		assertTrue("Get Definition not found", helpers.contains(new Help("calc", new ArrayList<>())));
	}
	
	@Test
	public void calc() throws IOException {
		assertEquals("285", ftpController.calc(null));
	}
}
