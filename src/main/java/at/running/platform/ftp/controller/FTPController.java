package at.running.platform.ftp.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import at.running.platform.common.Command;
import at.running.platform.common.model.Help;

@CrossOrigin(origins = {"http://localhost:8080"})
@Controller
public class FTPController extends Command {
	
	private final static String CONTROLLER_NAME = "ftp";
	
	@PostMapping("/" + CONTROLLER_NAME + "/calc")
    @ResponseBody
    public String calc(HttpEntity<String> httpEntity) throws IOException {
		if (httpEntity != null) {
			System.out.println(httpEntity.getBody());
		}
		return "285";
	}

	@GetMapping("/" + CONTROLLER_NAME + "/help")
    @ResponseBody
    public List<Help> help() {
		return getHelperMethods(this.getClass().getMethods());
	}
}
